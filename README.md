# my-guix-channel

This is my own guix channel with some packages I use. I am willing to
add them the main channel eventually.


# license

I am really sorry if I specified some projects license incorrectly. I
didn't mean to, I do respect everyone's job.

If you find some license problems please let me know ASAP.
