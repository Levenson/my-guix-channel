;;; networking.scm ---
;;
;; Copyright (C) 2020 Alexey Abramov <levenson@mmer.org>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;; Code:


(define-module (abralek gnu services networking)
  #:use-module ((ice-9 ftw) #:select (scandir))
  #:use-module ((ice-9 textual-ports))
  #:use-module (guix gexp))

(define-public %snx-vpn-hosts
  "\
172.17.0.3 snx-obo-vpn
172.17.0.2 snx-vpn
")

(define-public %abralek-nftables-ruleset
  (plain-file "nftables.conf"
              "# Regular rules and docker rules
table inet filter {
	chain input {
		type filter hook input priority filter; policy drop;
		ct state invalid drop
		ct state { established, related } accept
		iifname lo accept
		ip protocol icmp accept
		ip6 nexthdr ipv6-icmp accept
		tcp dport 22 accept

                ip saddr 192.168.1.0/24 tcp dport 22000 accept # Syncthing actual listening port
                ip saddr 192.168.1.0/24 udp dport 21027 accept # Syncthing discovery

		reject
	}

	chain forward {
		# meta nftrace set 1
		type filter hook forward priority filter; policy accept;
		jump docker-user
		jump docker-isolation-stage-1
		oif docker0 ct state { established, related } counter accept
		oif docker0 jump docker
		oif docker0 iif != docker0 accept
		oif docker0 iif docker0 meta nftrace set 1 accept
	}

	chain output {
		type filter hook output priority filter; policy accept;
		iif docker0 counter meta nftrace set 1 accept
	}

	chain docker {
	}

	chain docker-isolation-stage-1 {
		iif docker0 oif != docker0 jump docker-isolation-stage-2
		return
	}

	chain docker-isolation-stage-2 {
		oif docker0 drop
                ip saddr 172.17.0.2/32 counter accept # snx-vpn
                ip saddr 172.17.0.3/32 counter accept # snx-obo-vpn
		return
	}

	chain docker-user {
		return
	}
}

table inet docker-nat {
	chain prerouting {
		# meta nftrace set 1
		type nat hook prerouting priority filter; policy accept;
		fib daddr type local jump docker
	}

	chain output {
		type nat hook output priority filter; policy accept;
		ip daddr != 127.0.0.0/8 fib daddr type local jump docker
	}

	chain postrouting {
		type nat hook postrouting priority filter; policy accept;
		oif != docker0 ip saddr 172.17.0.0/16 counter masquerade
	}

	chain docker {
		iif docker0 return
	}
}
"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; networking.scm ends here
