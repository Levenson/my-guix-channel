;;; davmail.scm ---
;;
;; Author:
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;; Code:

(define-module (abralek gnu services mail)
  #:use-module (guix)
  #:use-module (gnu services)
  #:use-module (gnu packages mail)
  #:use-module (gnu services mcron)
  #:use-module (gnu services mail)
  #:use-module (guix gexp))

(define-public dovecot-configuration-delta
  (dovecot-configuration
   ;; (dovecot dovecot+pigeonhole)
   (auth-mechanisms '("plain"))

   (postmaster-address "levenson+delta@mmer.org")

   (debug-log-path "/var/log/dovecot-debug.log")
   (mail-attribute-dict "file:%h/dovecot-attributes")
   (mail-location "maildir:/home/%u/Maildir:INBOX=~/Maildir/INBOX:LAYOUT=fs")

   (mail-plugin-dir "/etc/dovecot/modules")
   (mail-plugins '("fts" "fts_lucene"))

   (services
    (list
     (service-configuration
      (kind "managesieve-login")
      (listeners
       (list
        (inet-listener-configuration
         (protocol "sieve")
         (port 4190)
         (ssl? #f)))))))

   (protocols
    (list

     (protocol-configuration
      (name "imap")
      (mail-plugins '("$mail_plugins" "imap_sieve"))
      (imap-metadata? #t))

     (protocol-configuration
      (name "lmtp")
      (mail-plugins '("$mail_plugins" "sieve")))

     (protocol-configuration
      (name "sieve")
      (managesieve-notify-capability '("mailto"))
      (managesieve-sieve-capability
       '("body"
         "comparator-i;ascii-numeric"
         "copy"
         "date"
         "duplicate"
         "encoded-character"
         "enotify"
         "envelope"
         "environment"
         "extracttext"
         "fileinto"
         "foreverypart"
         "ihave"
         "imap4flags"
         "include"
         "index"
         "mailbox"
         "mime"
         "regex"
         "reject"
         "relational"
         "subaddress"
         "vacation"
         "variables"
         )))))

   (plugin-configuration
    (plugin-configuration
     (entries (list (cons 'fts "lucene")
                    (cons 'fts-autoindex "yes")
                    (cons 'fts-lucene "whitespace_chars=@.")
                    (cons 'mailbox-alias-old "Sent")
                    (cons 'mailbox-alias-new "Sent Message")
                    (cons 'mailbox-alias-old2 "Sent")
                    (cons 'mailbox-alias-new2 "Sent Items")

                    (cons 'sieve-plugins "sieve_imapsieve sieve_extprograms")
                    (cons 'imapsieve-url "sieve://sieve.delta.localhost")
                    (cons 'imapsieve-mailbox1-name "work.*")
                    (cons 'imapsieve-mailbox1-causes "COPY APPEND")
                    (cons 'imapsieve-mailbox1-before "file:/home/%u/sieve/work.sieve")
                    (cons 'sieve-global-extensions "+vnd.dovecot.environment +vnd.dovecot.debug")

                    (cons 'sieve "file:~/sieve;active=~/.dovecot.sieve")
                    ;; (cons 'sieve-default "/var/lib/dovecot/sieve/default.sieve")
                    (cons 'sieve-plugins "sieve_imapsieve")
                    (cons 'recipient-delimeter "+")
                    (cons 'sieve-quota-max-storage "50M")
                    ))))

   (namespaces
    (list
     (namespace-configuration
      (name "inbox")
      (inbox? #t)
      (list? #t)
      (mailboxes
       (list
        (mailbox-configuration (name "Archive") (special-use '("\\Archive")))
        (mailbox-configuration (name "Drafts") (special-use '("\\Drafts")))
        (mailbox-configuration (name "Junk") (special-use '("\\Junk")))
        (mailbox-configuration (name "Trash") (special-use '("\\Trash")))
        (mailbox-configuration (name "Sent") (special-use '("\\Sent")))
        (mailbox-configuration (name "Sent Messages") (special-use '("\\Sent"))))))))))

(define opensmtpd-conf
  (mixed-text-file "smtpd.conf" "
listen on lo port 25

action receive lmtp \"/var/run/dovecot/lmtp\"

action outbound              relay host \"smtp://mx4.mail.mmer.org\"
action outbound-davmail-lgi  relay host \"smtp://127.0.0.1:1025\"
action outbound-davmail-epam relay host \"smtp://127.0.0.1:2025\"

match for domain mmer.org          action outbound
match for domain epam.com          action outbound-davmail-epam
match for domain libertyglobal.com action outbound-davmail-lgi

match for local action receive
"))

(define-public smtp-service-delta
  (service opensmtpd-service-type
           (opensmtpd-configuration
            (config-file opensmtpd-conf))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; davmail.scm ends here
