(define-module (abralek gnu packages wm)
  #:use-module (gnu packages lisp-xyz)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages)
  #:use-module (guix build-system asdf)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils))

(define-public sbcl-clx-xkeyboard
  (let ((revision "1")
        (commit "11455d36283ef31c498bd58ffebf48c0f6b86ea6"))
    (package
      (name "sbcl-clx-xkeyboard")
      (version (git-version "0.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/filonenko-mikhail/clx-xkeyboard.git")
               (commit commit)))
         (file-name (string-append name "-" version "-checkout"))
         (sha256
          (base32 "1nxky9wsmm7nmwz372jgb4iy0ywlm22jw0vl8yi0k9slsfklvcqi"))))
      (arguments
       `(#:asd-system-name "xkeyboard"))
      (inputs
       `(("sbcl-clx" ,sbcl-clx)))
      (build-system asdf-build-system/sbcl)
      (home-page "https://github.com/filonenko-mikhail/clx-xkeyboard.git")
      (synopsis "XKeyboard is X11 extension for clx of the same name")
      (description "XKeyboard is X11 extension for clx of the same name")
      (license license:expat))))

(define-public cl-clx-xkeyboard
  (sbcl-package->cl-source-package sbcl-clx-xkeyboard))

(define slynk-systems (@@ (gnu packages lisp-xyz) slynk-systems))

(define-public stumpwm-slynk+xkeyboard
  (package (inherit stumpwm)
    (name "stumpwm-with-slynk-xkeyboard")
    (outputs '("out"))
    (inputs
     `(("gnome-session", gnome-session)
       ("stumpwm" ,stumpwm "lib")
       ("slynk" ,sbcl-slynk)
       ("trivial-gray-streams" ,sbcl-trivial-gray-streams)
       ("closer-mop", sbcl-closer-mop)
       ("clx-xkeyboard" ,sbcl-clx-xkeyboard)))
    (arguments
     (substitute-keyword-arguments (package-arguments stumpwm)
       ((#:phases phases)
        `(modify-phases ,phases
           (replace 'build-program
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (program (string-append out "/bin/stumpwm")))
                 (build-program program outputs
                                #:entry-program '((stumpwm:stumpwm) 0)
                                #:dependencies '("stumpwm"
                                                 "trivial-gray-streams"
                                                 "closer-mop"
                                                 "xkeyboard"
                                                 ,@slynk-systems)
                                #:dependency-prefixes
                                (map (lambda (input) (assoc-ref inputs input))
                                     '("stumpwm" "slynk" "closer-mop" "trivial-gray-streams" "clx-xkeyboard")))
                 ;; Remove unneeded file.
                 (delete-file (string-append out "/bin/stumpwm-exec.fasl"))
                 #t)))
           (add-after 'create-desktop-file 'create-gnome-session
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (xsessions (string-append out "/share/xsessions")))
                 (mkdir-p xsessions)
                 (call-with-output-file
                     (string-append xsessions "/gnome-stumpwm.desktop")
                   (lambda (file)
                     (format file
                             "[Desktop Entry]~@
                              Type=Application~@
                              Comment=StumpWM on GNOME, registers with gnome session manager.~@
                              Exec=~a --builtin --session=stumpwm~@
                              Name=Stumpwm with GNOME Session~@
                              X-GNOME-Autostart-Notify=False~@
                              X-GNOME-Autostart-Phase=DisplayServer~@
                              X-GNOME-Provides=windowmanager~@
                              X-GNOME-Autorestart=false~%"
                             (which "gnome-session"))))
                 #t)))
           (delete 'copy-source)
           (delete 'build)
           (delete 'check)
           (delete 'create-asd-file)
           (delete 'cleanup)
           (delete 'create-symlinks)))))))
