;;; emacs-xyz.scm ---
;;
;; Author: Alexey Abramov <levenson@mmer.org>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;; Code:

(define-module (abralek gnu packages emacs-xyz)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages code)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages rust-apps)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages mail)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages tex)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages wordnet)
  #:use-module (guix build utils)
  #:use-module (guix build-system emacs)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix licenses)
  #:use-module (guix packages)
  #:use-module (guix))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-public emacs-ts
  (let ((commit "6af3f272070843cfbf62f4a5ed6833c07339673d")
        (revision "3"))
    (package
      (name "emacs-ts")
      (version (git-version "0.1" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/Levenson/ts.el")
                      (commit commit)))
                (sha256
                 (base32
                  "1ys6ag8961l47x771p1x2jsda7ip80vif0icxgn8kh6caicc0007"))
                (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-s" ,emacs-s)
         ("emacs-dash" ,emacs-dash)))
      (arguments
       ;; XXX: Three tests are failing because of a timezone-related issue
       ;; with how they're written.  On my machine, all the failing test
       ;; results are 18000 seconds (5 hours) off.

       ;; The ts-parse-org function accepts a string without any timezone
       ;; info, not assumed to be in Unix time, and converts it to a so-called
       ;; ts struct.  The ts-unix function (accessor) accepts a ts struct,
       ;; then seems to assume the struct's corresponding time is in terms of
       ;; the user's current time zone, before returning a Unix time in
       ;; seconds.

       ;; The failing tests all have similar problems, but nothing else about
       ;; the library seems particularly off.

       `(#:tests? #t
         #:test-command '("emacs" "--batch"
                          "-l" "test/test.el"
                          "-f" "ert-run-tests-batch-and-exit")
         #:phases
         (modify-phases %standard-phases
           (add-before 'check 'make-tests-writable
             (lambda _
               (make-file-writable "test/test.el")
               #t))
           (add-before 'check 'delete-failing-tests
             (lambda _
               (emacs-batch-edit-file "test/test.el"
                 `(progn (progn
                          (goto-char (point-min))
                          (dolist (test-regexp '("ert-deftest ts-format"
                                                 "ert-deftest ts-parse-org\\_>"
                                                 "ert-deftest ts-parse-org-element"))
                                  (re-search-forward test-regexp)
                                  (beginning-of-line)
                                  (kill-sexp)))
                         (basic-save-buffer)))
               #t)))))
      (home-page "https://github.com/alphapapa/ts.el")
      (synopsis "Timestamp and date/time library")
      (description "This package facilitates manipulating dates, times, and
timestamps by providing a @code{ts} struct.")
      (license gpl3+))))


(define-public emacs-language-detection
  (let ((commit "54a6ecf55304fba7d215ef38a4ec96daff2f35a4")
        (revision "1"))
    (package
      (name "emacs-language-detection")
      (version (git-version "20161123" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/andreasjansson/language-detection.el.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "0p8kim8idh7hg9398kpgjawkxq9hb6fraxpamdkflg8gjk0h5ppa"))))
      (build-system emacs-build-system)
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           ;; package provides autoloads
           (delete 'make-autoloads))))
      (synopsis "Automatic language detection from code snippets")
      (home-page "https://github.com/andreasjansson/language-detection.el")
      (description "Emacs Lisp library that automatically detects the
programming language in a buffer or string. Implemented as a random
forest classifier, trained in scikit-learn and deployed to Emacs
Lisp.")
      (license gpl3+))))

(define-public emacs-ox-jira
  (let ((commit "db2ec528f46c9e611624ba28611c440a99bff255")
        (revision "1"))
    (package
      (name "emacs-ox-jira")
      (version (git-version "20171001" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/stig/ox-jira.el.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "04zz6359xkn4w7jmmadxyvjd8pw21gw12mqwch1l9yxc4m9q474l"))))
      (build-system emacs-build-system)
      (home-page "https://github.com/stig/ox-jira.el")
      (synopsis "Org-mode export backend for JIRA markup")
      (description "This module plugs into the regular Org Export
Engine and transforms Org files to JIRA markup for pasting into JIRA
tickets & comments.")
      (license gpl3+))))

(define-public emacs-ejira
  (let ((commit "9ef57f96456f0bb3be17befb000d960f5ac766b4")
        (revision "2"))
    (package
      (name "emacs-ejira")
      (version (git-version "20181212" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/nyyManni/ejira.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "056k1wczaqkvqx24hfcjfixknr51aqk2fmy7kgrsvhygw7b6gcla"))))
      (build-system emacs-build-system)
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           ;; package provides autoloads
           (delete 'make-autoloads))))
      (inputs
       `(("emacs-ox-jira" ,emacs-ox-jira)
         ("emacs-language-detection" ,emacs-language-detection)
         ("emacs-request" ,emacs-request)
         ("emacs-helm" ,emacs-helm)
         ("emacs-s" ,emacs-s)))
      (home-page "https://github.com/nyyManni/ejira")
      (synopsis "Use Jira in Emacs org-mode")
      (description "Use Jira in Emacs org-mode")
      (license gpl3+))))

(define-public emacs-historian
  (let ((commit "6be869f585b854eb849303c452ab4f91dab04fa9")
        (revision "1"))
    (package
      (name "emacs-historian")
      (version (git-version "20170111" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/PythonNut/historian.el")
                      (commit commit)))
                (file-name (string-append name "-" version ".tar.gz"))
                (sha256
                 (base32
                  "07iw04aibmiz5fn97dafyk5k67yl525w6i1gwzazil4wb81q4b21"))))
      (arguments
       `(#:exclude '("ivy-historian\\.el$"
                     "helm-flx-historian\\.el")))
      (inputs
       `(("emacs-helm" ,emacs-helm)))
      (build-system emacs-build-system)
      (home-page "https://github.com/PythonNut/historian.el")
      (synopsis "Uses historical completions for more better position
in the candindates list")
      (description "Historian.el stores the results of completing-read
and similar functions persistently.  This provides a way to give
completion candidates that are more frequently or more recently used a
better position in the candidates list")
      (license gpl3+))))

(define-public emacs-helm-flx
  (let ((commit "6640fac5cb16bee73c95b8ed1248a4e5e113690e")
        (revision "1"))
    (package
      (name "emacs-helm-flx")
      (version (git-version "20151013" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/PythonNut/helm-flx")
                      (commit commit)))
                (file-name (string-append name "-" version ".tar.gz"))
                (sha256
                 (base32
                  "1fh1dy6xpc476hs87mn9fwxhxi97h7clfnnm7dxb7hg43xmgsjjs"))))
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           ;; package provides autoloads
           (delete 'make-autoloads))))
      (inputs
       `(("emacs-helm" ,emacs-helm)
         ("emacs-flx" ,emacs-flx)))
      (build-system emacs-build-system)
      (home-page "https://github.com/PythonNut/helm-flx")
      (synopsis "This package implements intelligent helm fuzzy sorting, provided by flx")
      (description "This package implements intelligent helm fuzzy sorting, provided by flx")
      (license gpl3+))))

(define-public emacs-helm-flx-historian
  (let ((commit "6be869f585b854eb849303c452ab4f91dab04fa9")
        (revision "1"))
    (package
      (name "emacs-helm-flx-historian")
      (version (git-version "20170111" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/PythonNut/historian.el")
                      (commit commit)))
                (file-name (string-append name "-" version ".tar.gz"))
                (sha256
                 (base32
                  "07iw04aibmiz5fn97dafyk5k67yl525w6i1gwzazil4wb81q4b21"))))
      (arguments
       ;; Not interested in ivy
       `(#:exclude '("ivy-historian\\.el$"
                     "^historian\\.el")))
      (inputs
       `(("emacs-helm" ,emacs-helm)
         ("emacs-flx" ,emacs-flx)
         ("emacs-historian" ,emacs-historian)))
      (build-system emacs-build-system)
      (home-page "https://github.com/PythonNut/historian.el")
      (synopsis "Uses historical completions for more better position
in the candindates list")
      (description "Historian.el stores the results of completing-read
and similar functions persistently.  This provides a way to give
completion candidates that are more frequently or more recently used a
better position in the candidates list")
      (license gpl3+))))

(define-public emacs-helm-rg
  (package
    (name "emacs-helm-rg")
    (version "0.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/cosmicexplorer/helm-rg.git")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1k9yv9iw694alf5w7555ygk2i1b26i90rqq7ny63a4nd3y5cbs5f"))))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         ;; package provides autoloads
         (delete 'make-autoloads))))
    (inputs
     `(("emacs-dash" ,emacs-dash)
       ("emacs-helm" ,emacs-helm)))
    (build-system emacs-build-system)
    (home-page "https://github.com/cosmicexplorer/helm-rg")
    (synopsis "Search massive codebases extremely fast, using ripgrep and helm")
    (description "Search massive codebases extremely fast, using ripgrep and helm")
    (license gpl3+)))

(define-public emacs-langtool
  (let ((commit "0b8be27372cf69135dad8e64ccbc7c53da2c50f8")
        (revision "1"))
    (package
      (name "emacs-langtool")
      (version (git-version "2.0.2" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/mhayashi1120/Emacs-langtool.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "0xabb4hq5ddisnnngv0prka2dhwrz95pjypjpbn7022h4asvmw58"))))
      (build-system emacs-build-system)
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           ;; package provides autoloads
           (delete 'make-autoloads))))
      (home-page "https://github.com/mhayashi1120/Emacs-langtool.git")
      (synopsis "Langtool emacs plugin")
      (description "LanguageTool is a free proofreading software for
English, German, Spanish, Russian, and more than 20 other languages.")
      (license gpl3+))))

(define-public emacs-org-mime
  (let ((commit "1e792ef0616069b3ec74a4b7d96fced8c9c6eb8a")
        (revision "1"))
    (package
      (name "emacs-org-mime")
      (version (git-version "0.1.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/org-mime/org-mime.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "182ifw3rdblmk6hrrybmz7g6dm9k4kxnqg89drmicfy0qvn4h059"))))
      (build-system emacs-build-system)
      (inputs
       `(("emacs-org" ,emacs-org)))
      (home-page "https://github.com/org-mime/org-mime.git")
      (synopsis "Send HTML email using Org-mode")
      (description "Send HTML email using Org-mode")
      (license gpl3+))))

(define-public emacs-selected
  (let ((commit "03edaeac90bc6000d263f03be3d889b4685e1bf7")
        (revision "1"))
    (package
      (name "emacs-selected")
      (version (git-version "20170222" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/Kungsgeten/selected.el.git")
                      (commit "03edaeac90bc6000d263f03be3d889b4685e1bf7")))
                (file-name (git-file-name name revision))
                (sha256
                 (base32
                  "1d72vw1dcxnyir7vymr3cfxal5dndm1pmm192aa9bcyrcg7aq39g"))))
      (build-system emacs-build-system)
      (home-page "https://github.com/Kungsgeten/selected.el")
      (synopsis "The library provides the selected-minor-mode for Emacs")
      (description "When selected-minor-mode is active, the keybindings
in selected-keymap will be enabled when the region is active.

This is useful for commands that operates on the region, which you
only want bound to a key when the region is active. selected.el also
provides selected-global-mode, if you want selected-minor-mode in
every buffer.")
      (license expat))))

(define-public emacs-parinfer-mode-smart
  (let ((commit "3178c8fac2a2b3ff8364c2be7a79a8b08d5f9cb6")
        (revision "5"))
    (package (inherit emacs-parinfer-mode)
             (name "emacs-parinfer-mode")
             (version (git-version "0.4.10-smart" revision commit))
             (source (origin
                       (method git-fetch)
                       (uri (git-reference
                             (url "https://github.com/DogLooksGood/parinfer-mode.git")
                             (commit commit)))
                       (file-name (git-file-name name version))
                       (sha256
                        (base32
                         "0z1nv5mkmv8ml69rr9mavzn3vwwqcp7584idgisalf7xj3qrcfj8"))))
             (arguments
              `(#:include '("parinfer-smart.el")
                          #:phases
                          (modify-phases %standard-phases
                            ;; It uses if-let* macro and somehow it's not
                            ;; compiling with out explicit load.
                            (add-after 'unpack 'load-emacs-extra-libs
                              (lambda _
                                (substitute* "parinfer-smart.el"
                                  (("\\(require 'selected\\)")
                                   (string-append "(require 'cl-lib)\n"
                                                  "(require 'selected)\n"
                                                  "(require 'rainbow-delimiters)\n"
                                                  "(require 'subr-x)\n"
                                                  "(require 'seq)\n"))))))))
             (propagated-inputs
              `(("emacs-selected" ,emacs-selected)
                ("emacs-paredit" ,emacs-paredit)
                ("emacs-rainbow-delimiters" ,emacs-rainbow-delimiters)))
             (synopsis "Lisp structure editing mode. It is a smart version of the parinfer-mode"))))

(define-public emacs-nord-theme
  (let ((commit "9ed7b9c2d1bb68ae86d06f97d215d2883e4ff0d2")
        (revision "0"))
    (package
      (name "emacs-nord-theme")
      (version (git-version "0.3.0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/arcticicestudio/nord-emacs.git")
                      (commit commit)))
                (sha256
                 (base32
                  "1i5z50xdn1qbfvy394j35d5bahsay56ngj2nxl4vlf88pdh0w0a8"))
                (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (home-page "https://github.com/arcticicestudio/nord-emacs.git")
      (synopsis "Nord color theme for Emacs")
      (description "Nord Emacs is a 16 colorspace theme build to run in
GUI- and terminal mode with support for many third-party syntax- and
UI packages.")
      (license expat))))

(define-public emacs-default-text-scale-next
  (let ((commit "512d701df5e2079cad33329184fd7683c3b0b0af")
        (revision "1"))
    (package (inherit emacs-default-text-scale)
      (name "emacs-default-text-scale")
      (version (git-version "0.2" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/purcell/default-text-scale")
                      (commit commit)))
                (file-name (string-append name "-" version "-checkout"))
                (sha256
                 (base32
                  "1zwrjlaxsxx7snyvyklhrchkbqg14lhr9xk7rhhik8fp4dy4f5yj")))))))

(define-public help-plus
  (package
   (name "emacs-help-plus")
   (version "0")
   (source
    (origin
     (method url-fetch)
     (uri "https://www.emacswiki.org/emacs/download/help+.el")
     (file-name (string-append "help+-" version ".el"))
     (sha256
      (base32
       "0n653sd6lrkz3b44r2ask0ac9v9a429vxsar7bj8hhl7jx59jamb"))))
   (build-system emacs-build-system)
   (synopsis "Enhance the Emacs help system")
   (home-page "https://www.emacswiki.org/emacs/InfoPlus")
   (description "Library help+.el enhances the ‘*Help*’ buffer by
providing links for libraries that are mentioned there – see
HelpModePlus.")
   (license gpl3+)))

(define-public help-mode-plus
  (package
   (name "emacs-help-mode-plus")
   (version "0")
   (source
    (origin
     (method url-fetch)
     (uri "https://www.emacswiki.org/emacs/download/help-mode+.el")
     (file-name (string-append "help-mode+-" version ".el"))
     (sha256
      (base32
       "0krvhfv9pm82kk34b7qlvkxaxivvlcakj4z03vhickdv90vfpgbf"))))
   (build-system emacs-build-system)
   (synopsis "Enhance the Emacs help system")
   (home-page "https://www.emacswiki.org/emacs/HelpPlus")
   (description "Extensions to help-mode.el")
   (license gpl3+)))

(define-public help-fns-plus
  (package
   (name "emacs-help-fns-plus")
   (version "0")
   (source
    (origin
     (method url-fetch)
     (uri "https://www.emacswiki.org/emacs/download/help-fns+.el")
     (file-name (string-append "help-fns+-" version ".el"))
     (sha256
      (base32
       "0l8dmzw7if73khglr3nxjkygl4qhmm6j4cnng8vsgpwwpy9vrmd7"))))
   (build-system emacs-build-system)
   (synopsis "Extensions to help-fns.el")
   (home-page "https://www.emacswiki.org/emacs/HelpPlus")
   (description "Extensions to help-fns.el.")
   (license gpl3+)))

(define-public emacs-fsm
  (package
    (name "emacs-fsm")
    (version "0.2.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://elpa.gnu.org/packages/"
                                  "fsm-" version ".el"))
              (sha256
               (base32
                "1jyxyqdbfl8nv7c50q0sg3w5p7whp1sqgi7w921k5hfar4d11qqp"))))
    (build-system emacs-build-system)
    (home-page "http://elpa.gnu.org/packages/fsm.html")
    (synopsis "fsm.el is an exercise in metaprogramming inspired by gen_fsm of Erlang/OTP")
    (description "It aims to make asynchronous programming in Emacs Lisp
easy and fun.  By \"asynchronous\" I mean that long-lasting tasks
don't interfer with normal editing.")
    (license gpl2+)))

(define-public emacs-url-http-ntlm
  (package
    (name "emacs-url-http-ntlm")
    (version "2.0.4")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://elpa.gnu.org/packages/"
                                  "url-http-ntlm-" version ".el"))
              (sha256
               (base32
                "1cakq2ykraci7d1gl8rnpv4f2f5ffyaidhqb1282g7i72adwmb98"))))
    (build-system emacs-build-system)
    (home-page "http://elpa.gnu.org/packages/url-http-ntlm.html")
    (synopsis "This package provides a NTLM handler for the URL package")
    (description "This package provides a NTLM handler for the URL package")
    (license gpl2+)))


(define-public emacs-excorporate
  (package
    (name "emacs-excorporate")
    (version "0.8.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://elpa.gnu.org/packages/"
                                  "excorporate-" version ".tar"))
              (sha256
               (base32
                "1k89472x80wsn14y16km5bgynmmd2kbdfhylb3cc17jvdn1xr53y"))))
    (build-system emacs-build-system)
    (propagated-inputs
     `(("emacs-url-http-ntlm" ,emacs-url-http-ntlm)
       ("emacs-fsm" ,emacs-fsm)))
    (home-page "https://elpa.gnu.org/packages/excorporate.html")
    (synopsis "Excorporate provides Exchange integration for Emacs")
    (description
     "Excorporate provides Exchange integration for Emacs")
    (license gpl2+)))


(define-public emacs-header2
  (package
    (name "emacs-header2")
    (version "2019.08.04")
    (source
     (origin
       (method url-fetch)
       (uri "https://www.emacswiki.org/emacs/download/header2.el")
       (file-name (string-append "header2-" version ".el"))
       (sha256
        (base32
         "1pj89i4qk770bbxwka1lbi9lwc7zc6vq8hawm1sikps44d0drsjw"))))
    (build-system emacs-build-system)
    (synopsis "Support for creation and update of file headers")
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         ;; package provides autoloads
         (delete 'make-autoloads))))
    (home-page "https://www.emacswiki.org/emacs/header2.el")
    (description "Library header2.el lets you define file headers for different types of
file (C, shell, EmacsLisp, and so on).

A header can have parts that are associated with updating functions, so
that whenever the file is saved (or some other event occurs), those
parts are updated in specific ways. How a given file header looks is
under your control.  You can even include source-control (e.g. RCS)
keywords that are expanded when the file is checked (saved).  The tool is
quite flexible.")
    (license gpl3+)))


(define-public emacs-helm-pages
  (package
    (name "emacs-helm-pages")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://github.com/david-christiansen/helm-pages/archive/"
                    version ".tar.gz"))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0fz4rwsqbmwk6phmmzif6gxfmvg78b4rpr84j9xb6apq2mlpb6hr"))))
    (propagated-inputs
     `(("emacs-helm" ,emacs-helm)))
    (build-system emacs-build-system)
    (home-page "https://github.com/david-christiansen/helm-pages")
    (synopsis "This packages introduces a command, helm-pages")
    (description "This packages introduces a command, helm-pages, that
displays the first lines of pages in the current buffer using Helm.")
    (license gpl3+)))


(define-public emacs-apiwrap
  (let ((commit "e4c9c57d6620a788ec8a715ff1bb50542edea3a6")
        (revision "1"))
    (package
      (name "emacs-apiwrap")
      (version (git-version "0.5" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/vermiculus/apiwrap.el")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "0xpb8mmssajy42r2h1m9inhv1chx19wkp5p0p63nwpk7mhjj8bis"))))
      (build-system emacs-build-system)
      (home-page "https://github.com/vermiculus/apiwrap.el")
      (synopsis "API-Wrap.el is a tool to interface with the APIs of your favorite services")
      (description "These macros make it easy to define efficient and
consistently-documented Elisp functions that use a natural syntax for
application development")
      (license gpl3+))))

(define-public emacs-ghub-plus
  (let ((commit "51ebffe549286b3c0b0565a373f44f4d64fc57af")
        (revision "1"))
    (package
      (name "emacs-ghub-plus")
      (version (git-version "0.3" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/vermiculus/ghub-plus.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "11fr6ri95a9wkc0mqrkhjxz1fm2cb52151fc88k73l93mggib3ak"))))
      (build-system emacs-build-system)
      (inputs
       `(("emacs-ghub" ,emacs-ghub)
         ("emacs-apiwrap" ,emacs-apiwrap)))
      (home-page "https://github.com/vermiculus/ghub-plus")
      (synopsis "GHub+ is a thick GitHub API client built using
API-Wrap.el on ghub")
      (description "This package is a thick client built on ghub, the
miniscule GitHub client.  Its aim is to provide the common
functionality most helpful for application development.

Since ghub+ is built on ghub, any and all features you find lacking in
ghub+ can be done with ghub without needing to dig into either
package’s internals.  However, ghub+ provides some macros you may find
helpful in development; see Extending for details.  If you find your
function to be particularly helpful or believe it to be a common use
case, please consider contributing it to the library")
      (license gpl3+))))

(define-public emacs-magithub
  (let ((commit "e5aaef889e362fc15e110cd58de064ff8db6d615")
        (revision "1"))
    (package
      (name "emacs-magithub")
      (version (git-version "0.1.7" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/vermiculus/magithub.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "044vyidsbz0q925y21y009672qj4lki7hzflljxzsxwpnbr9c7v5"))))
      (build-system emacs-build-system)
      (inputs
       `(("git" ,git)
         ("emacs-magit-popup" ,emacs-magit-popup)
         ("emacs-magit" ,emacs-magit)
         ("emacs-s" ,emacs-s)
         ("emacs-markdown-mode" ,emacs-markdown-mode)
         ("emacs-ghub-plus" ,emacs-ghub-plus)))
      (home-page "https://github.com/vermiculus/magithub")
      (synopsis "Magithub is a collection of interfaces to GitHub")
      (description "Magithub is a collection of interfaces to GitHub
integrated into Magit workflows as well as support for working
offline.")
      (license gpl3+))))

(define-public emacs-eredis
  (let ((commit "cfbfc25832f6fbc507bdd56b02e3a0b851a3c368")
        (revision "1"))
    (package
      (name "emacs-eredis")
      (version (git-version "0.9.6" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/justinhj/eredis.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "1f2f57c0bz3c6p11hr69aar6z5gg33zvfvsm76ma11vx21qilz6i"))))
      (build-system emacs-build-system)
      (inputs
       `(("emacs-dash" ,emacs-dash)))
      (home-page "https://github.com/justinhj/eredis.git")
      (synopsis "Emacs redis client")
      (description "eredis is a redis client written in Emacs Lisp")
      (license gpl3+))))

(define-public emacs-rs-gnus-summary
  (package
    (name "emacs-rs-gnus-summary")
    (version "0")
    (source
     (origin
       (method url-fetch)
       (uri "https://raw.githubusercontent.com/Levenson/rs-gnus-summary/master/rs-gnus-summary.el")
       (file-name (string-append "rs-gnus-summary-" version ".el"))
       (sha256
        (base32
         "0zasqa1hfmvz7k9k85hzrz9q5a4zbp1ih4aniw3vw4y1r6dhcq2x"))))
    (build-system emacs-build-system)
    (synopsis "Auxiliary summary mode commands for Gnus")
    (home-page "http://theotp1.physik.uni-ulm.de/~ste/comp/emacs/gnus/rs-gnus-summary.el")
    (description "Auxiliary summary mode commands for Gnus")
    (license gpl3+)))

(define-public emacs-simple-theme
  (package
    (name "emacs-simple-theme")
    (version "1.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://gitlab.com/Levenson/" name "/-/archive/" version
                                  "/" name "-" version ".tar.gz"))
              (sha256
               (base32
                "1ppdn5sg5qvah6c8c7c5191nm5pzik1vkh0mxjj045zslmrsd2f6"))))
    (build-system emacs-build-system)
    (home-page "https://gitlab.com/Levenson/emacs-simple-theme.git")
    (synopsis "Simple color theme for Emacs")
    (description "Very light theme, which changes the basic. The
idea is to keep it simple, and apply dark/light part in a separate
theme.")
    (license gpl3)))

(define-public emacs-weechat
  (let ((commit "8cbda2738149b070c09288df550781b6c604beb2")
        (revision "1"))
    (package
      (name "emacs-weechat")
      (version (git-version "0.5.0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/the-kenny/weechat.el.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "1i930jaxpva9s6y3fj3nny46b70g4mqdjl54mcv2rzj95bp4f908"))))
      (build-system emacs-build-system)
      (home-page "https://github.com/the-kenny/weechat.el.git")
      (inputs
       `(("emacs-s" ,emacs-s)
         ("emacs-tracking" ,emacs-tracking)))
      (synopsis "Chat via Weechat in Emacs")
      (description "Chat via Weechat in Emacs")
      (license gpl3+))))

(define-public emacs-bitwarden
  (let ((commit "e463d154f346e16fb66c331b6a751d2d11e8ef6e")
        (revision "1"))
    (package
      (name "emacs-bitwarden")
      (version (git-version "0.1.1" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/seanfarley/emacs-bitwarden.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "18clszv4rk2mza5f4i2cp1cdwwxf24wcpmnigizpgg11338snhfn"))))
      (build-system emacs-build-system)
      (home-page "https://github.com/seanfarley/emacs-bitwarden")
      (synopsis "Bitwarden command wrapper for Emacs")
      (description "Solve your password management problems
The easiest and safest way for individuals, teams, and business organizations to store, share, and sync sensitive data.")
      (license gpl3+))))

(define-public emacs-kurecolor
  (let ((commit "a27153f6a01f38226920772dc4917b73166da5e6")
        (revision "1"))
    (package
      (name "emacs-kurecolor")
      (version (git-version "20180401" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/emacsfodder/kurecolor.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "04av67q5841jli6rp39hav3a5gr2vcf3db4qsv553i23ffplb955"))))
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           ;; package provides autoloads
           (delete 'make-autoloads))))
      (build-system emacs-build-system)
      (home-page "https://github.com/emacsfodder/kurecolor.git")
      (synopsis "Color editing goodies for Emacs")
      (description "A collection of color tools aimed at those working
with (normal 6 digit) hex color codes, useful for CSS, Emacs themes,
etc. etc.")
      (license gpl2))))

(define-public emacs-ansible-vault
  (let ((commit "4fe490f524b79748c9f23026bb88d8f516b4ef40")
        (revision "1"))
    (package
      (name "emacs-ansible-vault")
      (version (git-version "v0.3.4" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/zellio/ansible-vault-mode.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "17kbjlssxa9b2fcp8vf2xs2k5y6jgpw277mj2gbv173b0i7v1fjd"))))
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           ;; package provides autoloads
           (delete 'make-autoloads))))
      (build-system emacs-build-system)
      (home-page "https://github.com/zellio/ansible-vault-mode.git")
      (synopsis "Minor mode for manipulating ansible-vault files")
      (description "Ansible Vault is a feature of ansible that allows you to keep sensitive
data such as passwords or keys in encrypted files, rather than as
plaintext in playbooks or roles. These vault files can then be
distributed or placed in source control.")
      (license gpl3))))

(define-public emacs-flymake
  (package
    (name "emacs-flymake")
    (version "1.0.8")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://elpa.gnu.org/packages/flymake-" version ".el"))
              (sha256
               (base32
                "1hqxrqb227v4ncjjqx8im3c4mhg8w5yjbz9hpfcm5x8xnr2yd6bp"))))
    (build-system emacs-build-system)
    (home-page "https://elpa.gnu.org/packages/flymake.html")
    (synopsis "A universal on-the-fly syntax checker")
    (description "Flymake is a minor Emacs mode performing on-the-fly syntax checks.

Flymake collects diagnostic information for multiple sources,
called backends, and visually annotates the relevant portions in
the buffer.

This file contains the UI for displaying and interacting with the
results produced by these backends, as well as entry points for
backends to hook on to.")
    (license gpl3)))

(define-public emacs-fzf
  (let ((commit "521d18933cb586337c4e34281bdc71ac07202c98")
        (revision "1"))
    (package
      (name "emacs-fzf")
      (version (git-version "v0.2" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/bling/fzf.el")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "0fpzjslbhhwvs4nh5dxj9cyxyiw6n8qmg76mvq73k5mc8pk7d4ir"))))
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           ;; package provides autoloads
           (delete 'make-autoloads))))
      (build-system emacs-build-system)
      (home-page "https://github.com/bling/fzf.el")
      (synopsis "An Emacs front-end for fzf")
      (description "fzf is a general-purpose command-line fuzzy
finder. It's an interactive Unix filter for command-line that can be
used with any list; files, command history, processes, hostnames,
bookmarks, git commits, etc.")
      (license gpl3))))

(define-public emacs-yequake
  (let ((commit "d18166e597414350117d0b82a29e509fc53c636d")
        (revision "1"))
    (package
      (name "emacs-yequake")
      (version (git-version "0.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/alphapapa/yequake.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "074wijjphnpch7bl3455apfkdv35b0iw4l4lzwj0jcxyfszviq9f"))))
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           ;; package provides autoloads
           (delete 'make-autoloads))))
      (build-system emacs-build-system)
      (inputs
       `(("dash" ,emacs-dash)))
      (home-page "https://github.com/alphapapa/yequake")
      (synopsis "Drop-down Emacs frames, like Yakuake")
      (description "This package provides configurable, drop-down
Emacs frames, similar to drop-down terminal windows programs, like
Yakuake. Each frame can be customized to display certain buffers in a
certain way, at the desired height, width, and opacity. The idea is to
call the yequake-toggle command from outside Emacs, using emacsclient,
by binding a shell command to a global keyboard shortcut in the
desktop environment. Then, with a single keypress, the desired Emacs
frame can be toggled on and off, showing the desired buffers.")
      (license gpl3))))

;; (define-public emacs-org-protocol-capture-html
;;   (let ((commit "23a1336cd88c14a6217c43282d34ad8c469c9500")
;;         (revision "1"))
;;     (package
;;       (name "emacs-org-protocol-capture-html")
;;       (version (git-version "0.0" revision commit))
;;       (source
;;        (origin
;;          (method git-fetch)
;;          (uri (git-reference
;;                (url "https://github.com/alphapapa/org-protocol-capture-html.git")
;;                (commit commit)))
;;          (file-name (git-file-name name version))
;;          (sha256
;;           (base32
;;            "074wijjphnpch7bl3455apfkdv35b0iw4l4lzwj0jcxyfszviq9f"))))
;;       ;; (arguments
;;       ;;  `(#:phases
;;       ;;    (modify-phases %standard-phases
;;       ;;      ;; package provides autoloads
;;       ;;      (delete 'make-autoloads))))
;;       (build-system emacs-build-system)
;;       (inputs
;;        `(("emacs-s" ,emacs-s)
;;          ;; org-protocol
;;          ("emacs-org" ,emacs-org)
;;          ("pandoc")

;;          ))
;;       (home-page "https://github.com/alphapapa/yequake")
;;       (synopsis "Drop-down Emacs frames, like Yakuake")
;;       (description "This package provides configurable, drop-down
;; Emacs frames, similar to drop-down terminal windows programs, like
;; Yakuake. Each frame can be customized to display certain buffers in a
;; certain way, at the desired height, width, and opacity. The idea is to
;; call the yequake-toggle command from outside Emacs, using emacsclient,
;; by binding a shell command to a global keyboard shortcut in the
;; desktop environment. Then, with a single keypress, the desired Emacs
;; frame can be toggled on and off, showing the desired buffers.")
;;       (license gpl3))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; emacs-xyz.scm ends here
