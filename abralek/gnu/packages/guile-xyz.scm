;;; guile.scm ---
;;
;; Author: Alexey Abramov <levenson@mmer.org>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;; Code:

(define-module (abralek gnu packages guile-xyz)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages)
  #:use-module (gnu packages guile)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system guile)
  #:use-module (guix utils))

(define-public guile-requests
  (let ((commit "f9032aaefde4d1e34a1b359ee9ceec2e06b88b58")
        (revision "1"))
    (package
      (name "guile-requests")
      (version (git-version "0.0.8" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/Levenson/guile-requests.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "0qs9sf0p04ysyyifhiyrwn7blz3qn7lsnhy7vvdnwp3w9fqz7z0k"))))
      (inputs
       `(("guile" ,guile-2.2)))
      (native-inputs
       `(("autoconf" ,autoconf)
         ("automake" ,automake)
         ("m4" ,m4)
         ("guile" ,guile-2.2)
         ("pkg-config" ,pkg-config)))
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           (delete 'check))))
      (build-system gnu-build-system)
      (synopsis "Guile http request client on steroids")
      (description "Guile-request is a library with useful features like
redirects etc.")
      (home-page "https://gitlab.com/Levenson/guile-requests.git")
      (license license:gpl3+))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; guile.scm ends here
